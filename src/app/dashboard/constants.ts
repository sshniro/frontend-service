
export const TABS = {
    WELCOME: "WELCOME",
    PURCHASES: "PURCHASES",
    SALES: "SALES",
    CATALOGUE: "CATALOGUE",
    FAVOURITE: "FAVOURITE"
};

export const PAGE_SIZE = 5;